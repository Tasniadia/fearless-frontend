function createCard(name, description, pictureUrl, start, end, location){
    return `
    <div class="card">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <h6 class="card-subtitle text-muted">${location}</h6>
              <p class="card-text">${description}</p>
              <div class="card-footer">${start} - ${end}</div>
            </div>
          </div>
          `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error("Error lol");
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const start = new Date(details.conference.starts).toDateString();
                const end = new Date(details.conference.ends).toDateString();
                const location = details.conference.location.name
                const html = createCard(name, description, pictureUrl, start, end, location);
                const column = document.querySelector('.col');
                column.innerHTML += html;
                const row = document.querySelector('.row');
                  row.appendChild(column);

              }
            }
            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name


            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok){
            //     const details = await detailResponse.json();
            //     console.log(details);
            //     const detail = document.querySelector('.card-text')
            //     detail.innerHTML = details.conference.description
            //     console.log(details)

            //     const imageTag= document.querySelector('.card-img-top')
            //     imageTag.src = details.conference.location.picture_url;

            // }
        }
    } catch (e){
        console.error('e', e);
    }
});
